# -*- coding: utf-8 -*-
import simple_draw as sd
from random import uniform


def draw_leaf(point, angle):
    """Рисует листочек в точке point под углом angle"""
    size_leaf = 5
    v1 = sd.get_vector(start_point=point, angle=angle - 30, length=size_leaf, width=1)
    v1.draw(color=sd.COLOR_GREEN)
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 30, length=size_leaf, width=1)
    v2.draw(color=sd.COLOR_GREEN)
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 150, length=size_leaf, width=1)
    v3.draw(color=sd.COLOR_GREEN)
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle + 210, length=size_leaf, width=1)
    v4.draw(color=sd.COLOR_GREEN)


def draw_bunches(point, angle, length, delta, level_bunches):
    if level_bunches < sd.random_number(a=1, b=5):  # b - регулируем разряжённость листьев
        return True

    width = max(1, int(level_bunches / 3))
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=(210, 105, 30))
    next_point = v1.end_point
    next_angle_right = angle - delta / 2
    next_angle_left = angle + delta / 2
    next_length_right = length * uniform(.55, .85)
    next_length_left = length * uniform(.55, .85)
    next_level_bunches = level_bunches - 1

    right_bunch = draw_bunches(point=next_point, angle=next_angle_right, length=next_length_right,
                               delta=delta,
                               level_bunches=next_level_bunches)
    left_bunch = draw_bunches(point=next_point, angle=next_angle_left, length=next_length_left,
                              delta=delta,
                              level_bunches=next_level_bunches)
    if right_bunch or left_bunch:
        draw_leaf(point=next_point, angle=angle - 65)
        draw_leaf(point=next_point, angle=angle)
        draw_leaf(point=next_point, angle=angle + 65)
    return False


root_point = sd.get_point(300, 30)
draw_bunches(point=root_point, angle=90 + sd.random_number(a=-5, b=5), length=80, delta=60, level_bunches=10)

sd.pause()

# TODO - Доработать функцию draw_bunches так, чтобы угол между смежными стволами был случаен
# TODO - В новом модуле сделать функцию draw, рисующую дерево в указанной координате (x, y)
