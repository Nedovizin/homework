# -*- coding: utf-8 -*-
import simple_draw as sd


def draw_triangle(base_point, angle, length_side, color=sd.COLOR_YELLOW, width=1):
    sides = 3
    delta_angle = 360 / sides
    points_array = [base_point]
    old_vec = base_point
    for i in range(sides - 1):
        v = sd.get_vector(start_point=old_vec, angle=angle + delta_angle * i, length=length_side - 1)
        points_array.append(v.end_point)
        old_vec = v.end_point
    sd.polygon(point_list=points_array, color=color, width=width)


point_1 = sd.get_point(150, 50)
draw_triangle(base_point=point_1, angle=45, length_side=200, color=sd.COLOR_DARK_RED, width=0)

sd.pause()

# TODO - Изучить код и написать функцию, которая рисует ёлку
# TODO - В новом модуле сделать функцию draw, рисующую ёлку в указанной координате (x, y)
