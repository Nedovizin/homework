import simple_draw as sd
def draw():
    sd.resolution = (1200, 600)

    y = 500
    x = 100
    while True:
        sd.clear_screen()
        for i in range(100, 1000, 100):
            point = sd.get_point(x + i, y + i)
            point1 = sd.get_point(x, y - i)
            point2 = sd.get_point(x + i, y + i)
            sd.snowflake(center=point, length=50)
            sd.snowflake(center=point1, length=25)
            sd.snowflake(center=point2, length=10)

        y -= 10
        if y < 50:
            y = 500
            x = 100
        x = x + 5

        sd.sleep(0.1)
        if sd.user_want_exit():
            break

    sd.pause()
draw()