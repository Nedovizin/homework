# -*- coding: utf-8 -*-

import simple_draw as sd


center_point = sd.get_point(300, 300)
radius_sun = 50

sd.circle(center_position=center_point, radius=radius_sun, width=0)
for angle in range(0, 360, 20):
    rad_oreol = radius_sun
    length_ray = radius_sun + 200
    start_point_ray_x = center_point.x + rad_oreol * sd.cos(angle)
    end_point_ray_x = center_point.x + length_ray * sd.cos(angle)
    start_point_ray_y = center_point.y + rad_oreol * sd.sin(angle)
    end_point_ray_y = center_point.y + length_ray * sd.sin(angle)
    sd.line(start_point=sd.get_point(start_point_ray_x, start_point_ray_y),
            end_point=sd.get_point(end_point_ray_x, end_point_ray_y),
            width=5)

sd.pause()

# TODO - В новом модуле сделать функцию draw, рисующую солнышко в координатах (x, y). Длину лучиков сделать случайной
