# -*- coding: utf-8 -*-
import simple_draw as sd

sd.resolution = (1200, 600)


def bubble(point, color=(255, 255, 0)):
    sd.circle(center_position=point, radius=50, width=2, color=color)


point = sd.get_point(100, 100)
bubble(point=point)

sd.pause()

# TODO - В новом модуле сделать функцию draw, рисующую пузырь (из трёх вложенных кругов) в координатах (x, y)
#  случайными цветами.
