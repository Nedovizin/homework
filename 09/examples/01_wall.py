# -*- coding: utf-8 -*-
import simple_draw as sd

sd.resolution = (1200, 600)

width, height = 130, 70
for y in range(5):
    offset = width / 2 if y % 2 == 0 else 0
    for x in range(5):
        pos_x = x * width + offset
        pos_y = y * height
        left_bottom_point = sd.get_point(pos_x, pos_y)
        right_top_point = sd.get_point(pos_x + width - 1, pos_y + height - 1)
        sd.rectangle(left_bottom=left_bottom_point, right_top=right_top_point, width=0, color=(255, 102, 26))
        sd.rectangle(left_bottom=left_bottom_point, right_top=right_top_point, width=2, color=(194, 194, 214))

sd.pause()

# TODO -  В новом модуле сделать функцию draw, рисующую дом: кирпичная стена, окно, дверь, треугольная крыша
#  Функция, которая пригодится для крыши: sd.polygon()08_smile.py
