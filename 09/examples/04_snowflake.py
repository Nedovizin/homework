# -*- coding: utf-8 -*-
import random
from random import uniform

import simple_draw as sd


snowflakes = [[random.randint(0, 1200), random.randint(700, 3000)] for _ in range(150)]


def draw_snowflake():
    sd.clear_screen()
    sd.start_drawing()
    for snowflake in snowflakes:
        point = sd.get_point(snowflake[0], snowflake[1])
        sd.snowflake(center=point, length=10)
        snowflake[1] -= uniform(1, 10)
        if snowflake[1] < 50:
            snowflake[1] = random.randint(500, 600)
            snowflake[0] = random.randint(0, 1200)
        snowflake[0] = snowflake[0] + uniform(-5, 5)
    sd.finish_drawing()
    sd.sleep(0.05)
