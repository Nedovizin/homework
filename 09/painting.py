# -*- coding: utf-8 -*-

# Собрать пакет drawing_package c модулями для отрисовки картины (примеры кода в папке examples)
#  - пузырьки (bubbles)
#  - дом (house)
#  - дерево (tree)
#  - смайлик (smile)
#  - снегопад (snowfall)
#  - ёлка (fir)
#  - солнце (sun)
# В каждом модуле должна быть функция draw(x, y).

# С помощью созданного пакета нарисовать эпохальное полотно "Утро в деревне".
# На картине должны быть:
#  - кирпичный дом, в окошке - смайлик.
#  - справа от дома - дерево (можно несколько)
#  - в небе сияет солнце
#  - добавить мыльных пузырьков для воздушности картинки
#  - при этом идёт снег
import random

import simple_draw as sd

from drawing_package import bubbles, house, tree, smile, snowfall, fir, sun

sd.resolution = (1200, 600)

sd.start_drawing()


for _ in range(20):
    bubbles.bubble(random.randint(40, 1160), random.randint(40, 560))

house.draw(150, 0)
smile.draw_smile(400, 125)
sun.draw_sun(1100, 550)
fir.draw_fir(200, 0)
fir.draw_fir(800, 0)
tree.draw_tree(100, 10)
tree.draw_tree(1000, 10)

sd.take_background()

while True:
    sd.start_drawing()
    # Начало отрисовки кадра
    sd.draw_background()

    snowfall.draw_snowflake()

    # Окончание отрисовки кадра и его вывод на экран
    sd.finish_drawing()
    # Пауза между кадрами
    sd.sleep(0.02)
    # Выход из программы, если пользователь нажал Esc
    if sd.user_want_exit():
        break
