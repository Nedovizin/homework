# -*- coding: utf-8 -*-
import simple_draw as sd


def draw_triangle(base_point, angle, length_side, color=sd.COLOR_YELLOW, width=1):
    sides = 3
    delta_angle = 360 / sides
    points_array = [base_point]
    old_vec = base_point
    for i in range(sides - 1):
        v = sd.get_vector(start_point=old_vec, angle=angle + delta_angle * i, length=length_side - 1)
        points_array.append(v.end_point)
        old_vec = v.end_point
    sd.polygon(point_list=points_array, color=color, width=width)


def draw_fir(x, y):
    point_1 = sd.get_point(x, y)
    draw_triangle(base_point=point_1, angle=0, length_side=200, color=sd.COLOR_DARK_GREEN, width=0)
    point_1 = sd.get_point(x+25, y+75)
    draw_triangle(base_point=point_1, angle=0, length_side=150, color=sd.COLOR_DARK_GREEN, width=0)
    point_1 = sd.get_point(x+50, y+135)
    draw_triangle(base_point=point_1, angle=0, length_side=100, color=sd.COLOR_DARK_GREEN, width=0)
    point_1 = sd.get_point(x+62, y+180)
    draw_triangle(base_point=point_1, angle=0, length_side=75, color=sd.COLOR_DARK_GREEN, width=0)


