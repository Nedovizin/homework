# -*- coding: utf-8 -*-
import simple_draw as sd
from random import uniform


def draw_sun(x, y):
    center_point = sd.get_point(x, y)
    radius_sun = 50

    sd.circle(center_position=center_point, radius=radius_sun, width=0)
    for angle in range(0, 360, 20):
        rad_oreol = radius_sun
        length_ray = radius_sun + uniform(10, 250)
        start_point_ray_x = center_point.x + rad_oreol * sd.cos(angle)
        end_point_ray_x = center_point.x + length_ray * sd.cos(angle)
        start_point_ray_y = center_point.y + rad_oreol * sd.sin(angle)
        end_point_ray_y = center_point.y + length_ray * sd.sin(angle)
        sd.line(start_point=sd.get_point(start_point_ray_x, start_point_ray_y),
                end_point=sd.get_point(end_point_ray_x, end_point_ray_y),
                width=3)


# center_point = sd.get_point(300, 300)
