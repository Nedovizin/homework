import simple_draw as sd


def draw(x1, y1):
    width, height = 130, 70
    for y in range(5):
        offset = width / 2 if y % 2 == 0 else width
        for x in range(5):
            pos_x = x * width + x1 + offset
            pos_y = y * height + y1
            if y % 2 == 0 and x == 0:
                left_bottom_point = sd.get_point(pos_x + width / 2, pos_y)
                right_top_point = sd.get_point(pos_x + width - 1, pos_y + height - 1)
            elif y % 2 != 0 and x == 4:
                right_top_point = sd.get_point(pos_x + width - 1 - width / 2, pos_y + height - 1)
                left_bottom_point = sd.get_point(pos_x, pos_y)
            else:
                left_bottom_point = sd.get_point(pos_x, pos_y)
                right_top_point = sd.get_point(pos_x + width - 1, pos_y + height - 1)
            sd.rectangle(left_bottom=left_bottom_point, right_top=right_top_point, width=0, color=(255, 102, 26))
            sd.rectangle(left_bottom=left_bottom_point, right_top=right_top_point, width=2, color=(194, 194, 214))
    point_list = [
        sd.get_point(0+x1, y1+3.5 * height),
        sd.get_point(4.5 / 2 * width +x1+ width, y1+8.5 * height),
        sd.get_point(x1+6.5 * width,y1+ 3.5 * height)
    ]
    sd.polygon(point_list=point_list, width=0, color='red')

    left_bottom_point = sd.get_point(1.5 * width + x1, height + y1)
    right_top_point = sd.get_point(2.5 * width + x1, 2.75 * height + y1)
    sd.rectangle(left_bottom= left_bottom_point, right_top=right_top_point, width=0,
                 color='blue')

    left_bottom_point = sd.get_point(4.5 * width + x1, y1)
    right_top_point = sd.get_point(5.25 * width + x1, 3 * height + y1)
    sd.rectangle(left_bottom=left_bottom_point, right_top=right_top_point, width=0,
                 color='brown')

