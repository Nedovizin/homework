# -*- coding: utf-8 -*-
import random

import simple_draw as sd
from random import uniform


def draw_leaf(point, angle):
    """Рисует листочек в точке point под углом angle"""
    size_leaf = 5
    color = random.choice([sd.COLOR_YELLOW, sd.COLOR_RED, sd.COLOR_GREEN])
    v1 = sd.get_vector(start_point=point, angle=angle - 30, length=size_leaf, width=1)
    v1.draw(color=color)
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 30, length=size_leaf, width=1)
    v2.draw(color=color)
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 150, length=size_leaf, width=1)
    v3.draw(color=color)
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle + 210, length=size_leaf, width=1)
    v4.draw(color=color)


def draw_bunches(point, angle, length, delta, level_bunches):
    if level_bunches < sd.random_number(a=-1, b=2):  # b - регулируем разряжённость листьев
        return True

    width = max(2, int(level_bunches / 2))
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=width)
    v1.draw(color=(210, 105, 30))
    next_point = v1.end_point
    next_angle_right = angle - delta / uniform(0.7, 7)
    next_angle_left = angle + delta / uniform(0.7, 6)
    next_length_right = length * uniform(.55, .99)
    next_length_left = length * uniform(.55, .95)
    next_level_bunches = level_bunches - 1

    right_bunch = draw_bunches(point=next_point, angle=next_angle_right, length=next_length_right,
                               delta=delta,
                               level_bunches=next_level_bunches)
    left_bunch = draw_bunches(point=next_point, angle=next_angle_left, length=next_length_left,
                              delta=delta,
                              level_bunches=next_level_bunches)
    if right_bunch or left_bunch:
        draw_leaf(point=next_point, angle=angle - 65)
        draw_leaf(point=next_point, angle=angle)
        draw_leaf(point=next_point, angle=angle + 65)
    return False


def draw_tree(x, y):
    root_point = sd.get_point(x, y)
    draw_bunches(point=root_point, angle=90 + sd.random_number(a=-5, b=5), length=80, delta=60, level_bunches=10)


if __name__ == "__main__":
    draw_tree(300, 10)

# draw_tree(150, 230)
