import random
import simple_draw as sd

sd.resolution = (1200, 600)


def draw_smile(x, y):
    color = (255, 255, 0)

    rad_x_half = 50
    rad_y_hlaf = 50

    left_bottom_point = sd.get_point(x - rad_x_half, y - rad_y_hlaf)
    right_top_point = sd.get_point(x + rad_x_half, y + rad_y_hlaf)
    sd.ellipse(left_bottom=left_bottom_point, right_top=right_top_point, color=color, width=1)
    left_bottom_eye = sd.get_point(x - rad_x_half / 2, y + rad_y_hlaf / 8)
    right_top_eye = sd.get_point(x - rad_x_half / 4, y + rad_y_hlaf / 1.3)
    sd.ellipse(left_bottom=left_bottom_eye, right_top=right_top_eye, color=color, width=0)
    left_bottom_eye = sd.get_point(x + rad_x_half / 4, y + rad_y_hlaf / 8)
    right_top_eye = sd.get_point(x + rad_x_half / 2, y + rad_y_hlaf / 1.3)
    sd.ellipse(left_bottom=left_bottom_eye, right_top=right_top_eye, color=color, width=0)
    point_list = [
        sd.get_point(x - rad_x_half / 2, y),
        sd.get_point(x + rad_x_half / 2, y),
        sd.get_point(x + rad_x_half / 3, y - rad_y_hlaf / 2),
        sd.get_point(x, y - rad_y_hlaf / 4 * 3),
        sd.get_point(x - rad_x_half / 3, y - rad_y_hlaf / 2),
    ]
    sd.polygon(point_list=point_list, width=0, color=color)


