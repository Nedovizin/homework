import simple_draw as sd
def bubble(x1,y1):
    point = sd.get_point(100 + x1, 100 + y1)
    sd.circle(center_position=point, radius=50, width=2, color='red')
    sd.circle(center_position=point, radius=100, width=2, color='pink')
    sd.circle(center_position=point, radius=25, width=2, color='yellow')