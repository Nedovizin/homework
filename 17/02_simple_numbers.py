# -*- coding: utf-8 -*-


# Есть функция генерации списка простых чисел


def get_simple_numbers(n):
    simple_numbers = []
    for number in range(2, n+1):
        for simple in simple_numbers:
            if number % simple == 0:
                break
        else:
            simple_numbers.append(number)
    return simple_numbers

# Часть 1
# На основе алгоритма get_simple_numbers создать класс итерируемых обьектов,
# который выдает последовательность простых чисел до n
#
# Распечатать все простые числа до 10000 в столбик


class SimpleNumbers:
    pass
    # TODO здесь ваш код


simple_number_iterator = SimpleNumbers(n=10000)
for number in simple_number_iterator:
    print(number)


# Часть 2
# Теперь нужно создать генератор, который выдает последовательность простых чисел до n
# Распечатать все простые числа до 10000 в столбик


def simple_numbers_generator(n):
    pass
    # TODO здесь ваш код


for number in simple_numbers_generator(n=10000):
    print(number)


# Усложненное задание (делать по желанию)
# Преобразовать итератор/генератор так, чтобы он выдавал только "счастливые" простые числа
# у которых сумма первых цифр равна сумме последних
# Если простое число имеет нечетное число цифр (например 727 или 92083),
# то для вычисления "счастливости" брать равное количество цифр с начала и конца:
#   727 -> 7(2)7 -> 7 == 7 -> True
#   92083 -> 92(0)83 -> 9+2 == 8+3 -> True


