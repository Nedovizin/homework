import time
import os


class HolderNumber:
    def __init__(self):
        self.num = 1


def write_to_console(num, *args, **kwargs):
    print(num, *args, **kwargs, flush=True)


def write_to_file(file_name):
    if os.path.exists(file_name):
        os.remove(file_name)

    def wrapper(num, *args, **kwargs):
        with open(file=file_name, mode="a", encoding="utf8") as file:
            file.write(f"{num} ")
            for arg in args:
                file.write(f"{arg} ")
            for k, v in kwargs.items():
                file.write(f"{k}={v} ")  # так нагляднее
            file.write("\n")
    return wrapper


def get_numbered_writer(writer):
    # Чтобы i была по ссылке.
    # В замыкании простое число не сохраняется
    _holder = HolderNumber()

    def write(*args, **kwargs):
        writer(_holder.num, *args, **kwargs)
        # print(_holder.num, *args, **kwargs, flush=True)
        _holder.num += 1

    return write


def time_track(func):
    def surrogate(*args, **kwargs):
        started_at = time.time()

        result = func(*args, **kwargs)

        ended_at = time.time()
        elapsed = round(ended_at - started_at, 4)
        print(f'Функция работала {elapsed} секунд(ы)')
        return result

    return surrogate


# print_numered_line = get_numbered_writer(write_to_file(file_name="threads.txt"))
# print_numered_line = get_numbered_writer(write_to_file(file_name="process.txt"))
