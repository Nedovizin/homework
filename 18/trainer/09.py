"""
Счётчик
Что нужно сделать

Реализуйте декоратор counter, считающий и выводящий количество вызовов декорируемой функции.

Для решения задачи нельзя использовать операторы global и nonlocal (об этом мы ещё расскажем).
"""
def counter(func):
    param = {'count': 0}
    def wrapper(*args, **kwargs):
        param['count'] += 1
        print(f"counter:{param['count']}")
        func(*args, **kwargs)
    return wrapper


@counter
def process():
    print("<<< i do >>>")


process()
process()
process()

