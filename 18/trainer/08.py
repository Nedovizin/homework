"""
Дебаг
Что нужно сделать

Напишите декоратор debug, который при каждом вызове декорируемой функции выводит её имя (вместе со всеми
передаваемыми аргументами), а затем — какое значение она возвращает. После этого выводится результат её выполнения.

Пример декорируемой функции:

def greeting(name, age=None):
    if age:
        return "Ого, {name}! Тебе уже {age} лет, ты быстро растёшь!".format(name=name, age=age)
    else:
        return "Привет, {name}!".format(name=name)


Основной код:

greeting("Том")
greeting("Миша", age=100)
greeting(name="Катя", age=16)


Результат:
Вызывается greeting('Том')
'greeting' вернула значение 'Привет, Том!'
Привет, Том!

Вызывается greeting('Миша', age=100)
'greeting' вернула значение 'Ого, Миша! Тебе уже 100 лет, ты быстро растёшь!'
Ого, Миша! Тебе уже 100 лет, ты вырос!

 Вызывается greeting(name='Катя', age=16)
'greeting' вернула значение 'Ого, Катя! Тебе уже 16 лет, ты быстро растёшь!'
Ого, Катя! Тебе уже 16 лет, ты быстро растешь!

Совет: попробуйте самостоятельно изучить функцию repr. Это поможет в решении задачи.
"""
def debug(func):
    def wrapper(*args, **kwargs):
        print_arg = [str(arg) for arg in args]
        print_arg.extend([f"{key}={value}" for key, value in kwargs.items()])
        print_all_args = ", ".join(print_arg)

        print(f"Вызывается функция {func.__name__}({print_all_args})")
        result = func(*args, **kwargs)
        print(f"'{func.__name__}' вернула значение '{result}'")
        print(result)
        return result
    return wrapper


@debug
def greeting(name, age=None):
    if age:
        return "Ого, {name}! Тебе уже {age} лет, ты быстро растёшь!".format(name=name, age=age)
    else:
        return "Привет, {name}!".format(name=name)


greeting("Том")
greeting("Миша", age=100)
greeting(name="Катя", age=16)
