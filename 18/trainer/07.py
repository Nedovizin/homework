"""
Логирование
Что нужно сделать

Реализуйте декоратор logging, который будет отвечать за логирование функций. На экран выводится название функции
и её документация. Если во время выполнения декорируемой функции возникла ошибка, то в файл function_errors.log
записываются названия функции и ошибки.

Также постарайтесь сделать так, чтобы программа не завершалась после обнаружения первой же ошибки, а обрабатывала
все декорируемые функции и сразу записывала все ошибки в файл.

Дополнительно: запишите дату и время возникновения ошибки, используя модуль datetime.
"""
from datetime import datetime


def logging(func):
    def wrapper(*args, **kwargs):
        print(func.__name__)
        print(func.__doc__)
        try:
            return func(*args, **kwargs)
        except Exception as exec:
            with open("function_errors.log", mode="a", encoding="utf8") as log_file:
                log_file.write(f"{datetime.now()} {func.__name__}, {exec}\n")
    return wrapper


@logging
def process1():
    """Это нектороый процесс, очень важный"""
    print("calc process 1")

@logging
def process_divider(n):
    """Делитель десяти"""
    return 10/n


process1()
print()
process_divider(0)
print()
process_divider(1)

