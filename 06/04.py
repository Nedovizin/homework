# Есть некоторый словарь, который хранит названия
# стран и столиц. Название страны используется в качестве
# ключа, название столицы в качестве значения. Необходимо
# реализовать: добавление данных, удаление данных, поиск
# данных, редактирование данных, сохранение и загрузку
# данных (используя упаковку и распаковку).

countries = {
    'Russia': 'Moskow'
}


def add(**kwargs):
    countries[kwargs['country']] = kwargs['capital']
    ...


def delete(*args):
    ...


def update(**kwargs):
    ...


def find_countries(*args):
    ...


add(country='France', capital='Paris')
print(countries)
delete('France', 'USA')
