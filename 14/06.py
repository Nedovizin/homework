"""
Паранойя
Что нужно сделать

Артуру постоянно кажется, что за ним следят и все хотят своровать «крайне важную информацию» с его компьютера,
включая переписку с людьми. Поэтому он эти переписки шифрует. И делает это с помощью шифра Цезаря (чем веселит агента
службы безопасности).

Напишите программу, которая шифрует содержимое текстового файла text.txt шифром Цезаря, при этом
символы первой строки файла должны циклически сдвигаться на один, второй строки — на два,
третьей строки — на три и так далее. Результат выведите в файл cipher_text.txt.

Пример:

Содержимое файла text.txt:
Hello
Hello
Hello
Hello
Zero

Содержимое файла cipher_text.txt:
Ifmmp
Jgnnq
Khoor
Lipps
"""
result = ""
with open("text.txt", "r") as file:
    for idx, line in enumerate(file, 1):
        for symbol in line.strip():
            isupper = symbol.isupper()
            code = ord(symbol.lower())
            next_code = code + idx
            if next_code > ord("z"):
                next_code = ord("a") + next_code - ord("z") - 1
            if isupper:
                result += chr(next_code).upper()
            else:
                result += chr(next_code)
        result += "\n"

with open("cipher_text.txt", mode="w") as file:
    file.write(result)
