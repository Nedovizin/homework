"""
Сумма чисел
Что нужно сделать

Во входном файле numbers.txt записано N целых чисел, которые могут быть разделены пробелами и концами строк.
Напишите программу, которая выводит сумму чисел в выходной файл answer.txt.

Пример:
Содержимое файла numbers.txt
  2   2
2
  23
        2

Содержимое файла answer.txt
31
"""

# line = "  2   20  \n"
# arr = line.strip().split()
# sum = sum([int(x) for x in arr])
# print(sum)
#
with open('numbers.txt', mode='r') as f:
    sum = 0
    for line in f:
        stroke_arr = line.strip().split()
        for number in stroke_arr:
            sum += int(number)
    print(sum)

with open('answer', mode='w') as answer:
    answer.write(str(sum))
