"""
Дзен Пайтона 2
Что нужно сделать

Напишите программу, которая определяет, сколько букв (латинского алфавита), слов и строк в файле zen.txt
(который содержит всё тот же Дзен Пайтона). Выведите три найденных числа на экран.

Далее: выведите на экран букву, которая встречается в тексте наименьшее количество раз.

Обратите внимание, что регистр буквы не имеет значение. (А и а - одинаковые буквы).

Формат вывода:
Количество букв в файле:
Количество слов в файле:
Количество строк в файле:
Наиболее редкая буква:
"""
# import operator
#
# letters = {}
# symbol = "a".lower()
# if symbol.isalpha():
#     ...
# # variant 1
# if symbol in letters:
#     letters[symbol] += 1
# else:
#     letters[symbol] = 1
#
# # variant 2
# letters[symbol] = letters.get(symbol, 0) + 1
#
# letters = {"a": 4, "b": 7}
# print(sorted(letters.items(), key=operator.itemgetter(1))[-1])

import operator
from pprint import pprint

file_name = 'zen.txt'
with open(file_name, mode='r') as file:
    letters = {}
    strokes = 0
    letters_count = 0
    word_counter = 0
    for stroke in file:
        strokes += 1
        word_counter += len(stroke.split())
        for symb in stroke.lower():
            if symb.isalpha():
                if symb in letters:
                    letters[symb] += 1
                else:
                    letters[symb] = 1
    for symb in letters:
        letters_count += letters[symb]

print(f"Строки: {strokes}")
print(f"Буквы: {letters_count}")
print(sorted(letters.items(), key=operator.itemgetter(1))[0][0])
print('слов: ', word_counter)
