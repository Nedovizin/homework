"""
Файлы и папки
Что нужно сделать

Напишите программу, которая получает на вход путь до каталога
(это может быть и просто корень диска) и выводит
общее количество файлов и подкаталогов в нём. Также выведите на экран размер
каталога в килобайтах
(1 килобайт = 1024 байт).

Важный момент: чтобы посчитать, сколько весит каталог, нужно найти сумму размеров
всех вложенных в него файлов.
"""
# path = os.path.join(folder_name, file_name)
# size = os.path.getsize(path)
from os.path import getsize, join
from os import walk

path = r'C:\Windows\Help'
total_size = 0
len_dirnames = 0
len_files = 0
for dirpath, dirnames, filenames in walk(path):
    len_dirnames += len(dirnames)
    len_files += len(filenames)
    for f in filenames:
        total_size += getsize(join(dirpath, f))

print('Всего файлов: ', len_files, ", ", int(total_size / 1024), 'kilobyte')
print('кол-во подкаталогов: ', len_dirnames)
