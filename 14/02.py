"""
Дзен Пайтона
Что нужно сделать

Сохранить в файл zen.txt так называемый Дзен Пайтона — текст философии программирования на языке Python.
Получить его можно так:
import this

Напишите программу, которая выводит на экран все строки этого файла в обратном порядке.

Результат работы программы:
Namespaces are one honking great idea -- let's do more of those!
If the implementation is easy to explain, it may be a good idea.
If the implementation is hard to explain, it's a bad idea.
Although never is often better than *right* now.
…
"""

# arr = ["1111", "22222", "33333"]
# for line in reversed(arr):
#     print(line)

file_name = 'zen.txt'
with open(file_name, mode='r') as file:
    new_ar = file.readline()
    print(*new_ar[::-1])

