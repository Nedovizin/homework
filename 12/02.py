"""
Задача 2. Студенты
Что нужно сделать

Реализуйте модель с именем Student, содержащую поля: «ФИ», «Номер группы», «Успеваемость» (список из пяти элементов).
Затем создайте список из десяти студентов (данные о студентах можете придумать свои или запросить их у пользователя)
и отсортируйте его по возрастанию среднего балла. Выведите результат на экран.

"""
from random import choice, randint


class Student:
    def __init__(self):
        self.fi = ""
        self.group = 0
        self.progress = []

    def avr_progress(self):
        if len(self.progress) == 0:
            return 0
        return sum(self.progress) / len(self.progress)

    def __str__(self):
        return f"{self.fi},\t{self.group},\t{self.avr_progress()}"


students = []
names = ["as", "ddddd", "ddfssds", "fgew4re", "fdf"]
for _ in range(10):
    student = Student()
    student.fi = choice(names)
    student.group = randint(1, 3)
    student.progress = [randint(1, 5) for x in range(5)]
    students.append(student)

students.sort(key=...)
for student in students:
    print(student)
