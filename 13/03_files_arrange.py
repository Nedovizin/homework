# -*- coding: utf-8 -*-

import os, time, shutil

# Нужно написать скрипт для упорядочивания фотографий (вообще любых файлов)
# Скрипт должен разложить файлы из одной папки по годам и месяцам в другую.
# Например, так:
#   исходная папка
#       photo/cat.jpg
#       photo/man.jpg
#       photo/new_year_01.jpg
#   результирующая папка
#       my_photo_archive/2018/05/cat.jpg
#       my_photo_archive/2018/05/man.jpg
#       my_photo_archive/2017/12/new_year_01.jpg
#
# Входные параметры: папка для сканирования, целевая папка
# Имена файлов не менять, год и месяц взять из времени создания файла.
# Исходная папка - icons, результирующая icons_by_years
#
# Пригодятся функции:
#   os.walk
#   os.path.dirname
#   os.path.join
#   os.path.normpath
#   os.path.getctime
#   time.gmtime
#   os.makedirs
#   shutil.copy2
#
# Чтение документации/гугла по функциям - приветствуется. Как и поиск альтернативных вариантов...

# TODO тут ваш код
