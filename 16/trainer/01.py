"""
Новые списки
Что нужно сделать

Даны три списка:

floats: List[float] = [12.3554, 4.02, 5.777, 2.12, 3.13, 4.44, 11.0001]
names: List[str] = ["Vanes", "Alen", "Jana", "William", "Richards", "Joy"]
numbers: List[int] = [22, 33, 10, 6894, 11, 2, 1]
Напишите код, который создаёт три новых списка.

Каждое число из списка floats возводится в третью степень и округляется до трёх знаков после запятой.
Из списка names берутся только те имена, в которых есть минимум пять букв.
Из списка numbers берётся произведение всех чисел.
"""
from functools import reduce


def mul(x, y):
    return x * y


numbers = [22, 33, 10, 6894, 11, 2, 1]
result3 = reduce(mul, numbers, 1)
print(result3)
