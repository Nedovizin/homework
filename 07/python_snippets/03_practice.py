# -*- coding: utf-8 -*-


# Ним — математическая игра, в которой два игрока по очереди берут предметы,
# разложенные на несколько кучек. За один ход может быть взято любое количество предметов
# (большее нуля) из одной кучки. Выигрывает игрок, взявший последний предмет.
# В классическом варианте игры число кучек равняется трём.

# Составить модуль, реализующий функциональность игры.
# Функции управления игрой
#  разложить_камни()
#  взять_из_кучи(NN, KK)
#  положение_камней() - возвращает список [XX, YY, ZZ, ...] - текущее расположение камней
#  есть_конец_игры() - возвращает True если больше ходов сделать нельзя
#
#
# В текущем модуле (lesson_006/python_snippets/04_practice.py) реализовать логику работы с пользователем:
#  начало игры,
#  вывод расположения камней
#  ввод первым игроком хода - позицию и кол-во камней
#  вывод расположения камней
#  ввод вторым игроком хода - позицию и кол-во камней
#  вывод расположения камней

from nim_engine import get_from_bunch, print_bunches, init_bunches, is_finish
from termcolor import colored


def main():
    init_bunches()
    while True:
        print()
        print_bunches()
        print(colored("Перовый игрок. Ваш ход", 'blue'))
        while True:
            user_1_bunch = int(input(colored("Номер кучки", 'blue')))
            user_1_stones = int(input(colored("количество камней", 'blue')))
            if get_from_bunch(user_1_bunch, user_1_stones):
                break
            print("... Некорректный ввод...")
        if is_finish():
            print("Первый игрок выиграл")
            break

        print()
        print_bunches()
        print(colored("Второй игрок. Ваш ход", 'green'))
        while True:
            user_1_bunch = int(input(colored("Номер кучки", 'green')))
            user_1_stones = int(input(colored("количество камней", 'green')))
            if get_from_bunch(user_1_bunch, user_1_stones):
                break
            print("... Некорректный ввод...")
        if is_finish():
            print("Второй игрок выиграл")
            break




main()
